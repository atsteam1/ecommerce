package controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import model.bean.Cliente;
import model.service.ServiceCliente;

@Controller
public class ControllerCliente {
	@Autowired
	private ServiceCliente sc;

	@RequestMapping("/LoginCliente")
	public ModelAndView login(HttpSession session, String user, String pssw) {
		Cliente c1 = null;

		try {
			c1 = sc.verificaCredenziali(user, pssw);
			if (c1 != null) {
				session.setAttribute("idCliente", c1.getId());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}

		return new ModelAndView("cliente");

	}

}
