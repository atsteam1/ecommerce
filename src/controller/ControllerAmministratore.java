package controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import model.bean.Amministratore;
import model.bean.Categoria;
import model.bean.Cliente;
import model.bean.Prodotto;
import model.service.ServiceAmministratore;

@Controller
public class ControllerAmministratore {

	@Autowired
	private ServiceAmministratore sa;

	@RequestMapping("/LoginAmministratore")
	public ModelAndView login(HttpSession session, String username, String password) {
		Amministratore am = null;

		try {
			am = sa.controlloAdmin(username, password);
			if (am != null) {
				session.setAttribute("idAmministratore", am.getId());
				return new ModelAndView("LoginAmministratore");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ModelAndView("errore");
	}

	@RequestMapping("/VisualizzaClienti")
	public ModelAndView VisualizzaClienti(HttpSession session) {

		List<Cliente> listaClienti = null;

		try {
			listaClienti = sa.visualizzaClienti();
		} catch (Exception e) {
			e.printStackTrace();
		}

		ModelAndView m = new ModelAndView("VisuliazzaClienti");

		m.addObject("ListaClienti", listaClienti);

		return m;

	}
	
	@RequestMapping("/VisualizzaSingoloCliente")
	public ModelAndView VisualizzaSingoloCliente(HttpSession session, int idCliente){
		
		Cliente cliente = null;
		
		ModelAndView m = new ModelAndView("VisuliazzaSingoloCliente");
		
		try {
			cliente = sa.visualizzaCliente(idCliente);
			if (cliente != null) {
				m.addObject("Cliente", cliente);
			}
			else{
				String messaggio = null;
				m.addObject(messaggio, "Il cliente cercato non esiste");
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return m;
		
	}

	@RequestMapping("/InserisciProdotto")
	public void InserisciProdotto(HttpSession session, Prodotto prodotto) {

		try {
			sa.inserisciProdotto(prodotto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping("/ModificaProdotto")
	public void ModificaProdotto(HttpSession session, Prodotto prodotto) {

		try {
			sa.modificaProdotto(prodotto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping("/EliminaProdotto")
	public void EliminaProdotto(HttpSession session, int idProdotto) {

		try {
			sa.eliminaProdotto(idProdotto);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping("/InserisciCategoria")
	public void InserisciCategoria(HttpSession session, Categoria categoria) {

		try {
			sa.inserisciCategoria(categoria);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping("/ModificaCategoria")
	public void ModificaCategoria(HttpSession session, Categoria categoria) {

		try {
			sa.modificaCategoria(categoria);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@RequestMapping("/EliminaCategoria")
	public void EliminaCategoria(HttpSession session, int Idcategoria) {

		try {
			sa.eliminaCategoria(Idcategoria);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}