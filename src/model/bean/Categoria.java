package model.bean;

import java.util.List;

public class Categoria {

	private int idCategoria;
	private String nome;
	private List<Prodotto> prodotti;
	
	public Categoria(int idCategoria, String nome, List<Prodotto> prodotti) {
		super();
		this.idCategoria = idCategoria;
		this.nome = nome;
		this.prodotti = prodotti;
	}
	
	

	public Categoria() {
		super();
	}



	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Prodotto> getProdotti() {
		return prodotti;
	}

	public void setProdotti(List<Prodotto> prodotti) {
		this.prodotti = prodotti;
	}
	
	
}
