package model.bean;

public class Cliente extends Utente {

	private boolean affidabile;
	private String tipologia;
	private String cf;
	private String nome;
	private String cognome;
	private String indirizzoCliente;
	private String partitaIva;
	private String indirizzoFatturazione;
	private int telefono;
	private int cellulare;
	private String email;
	
	public Cliente(int id, String username, String password, boolean amministratore, boolean affidabile,
			String tipologia, String cf, String nome, String cognome, String indirizzoCliente, String partitaIva,
			String ragioneSociale, String indirizzoFatturazione, int telefono, int cellulare, String email) {
		super(id, username, password, amministratore);
		
		this.affidabile = affidabile;
		this.tipologia = tipologia;
		this.cf = cf;
		this.nome = nome;
		this.cognome = cognome;
		this.indirizzoCliente = indirizzoCliente;
		this.partitaIva = partitaIva;
		this.indirizzoFatturazione = indirizzoFatturazione;
		this.telefono = telefono;
		this.cellulare = cellulare;
		this.email = email;
	}

	public Cliente() {
		super();
	}


	public boolean isAffidabile() {
		return affidabile;
	}
	
	public String getAffidabile(){
		if(this.isAffidabile()) return "s";
		else return "n";
	}

	public void setAffidabile(String s){
		if(s.equals("s"))
			this.setAffidabile(true);
		else
			this.setAffidabile(false);
	}
	public void setAffidabile(boolean affidabile) {
		this.affidabile = affidabile;
	}

	public String getTipologia() {
		return tipologia;
	}

	public void setTipologia(String tipologia) {
		this.tipologia = tipologia;
	}

	public String getCf() {
		return cf;
	}

	public void setCf(String cf) {
		this.cf = cf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCognome() {
		return cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public String getIndirizzoCliente() {
		return indirizzoCliente;
	}

	public void setIndirizzoCliente(String indirizzoCliente) {
		this.indirizzoCliente = indirizzoCliente;
	}

	public String getPartitaIva() {
		return partitaIva;
	}

	public void setPartitaIva(String partitaIva) {
		this.partitaIva = partitaIva;
	}


	public String getIndirizzoFatturazione() {
		return indirizzoFatturazione;
	}

	public void setIndirizzoFatturazione(String indirizzoFatturazione) {
		this.indirizzoFatturazione = indirizzoFatturazione;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public int getCellulare() {
		return cellulare;
	}

	public void setCellulare(int cellulare) {
		this.cellulare = cellulare;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	

	
	
}
