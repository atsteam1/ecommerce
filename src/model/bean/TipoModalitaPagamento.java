package model.bean;

public enum TipoModalitaPagamento {

	CONTANTI_CON_RITIRO_IN_NEGOZIO("Contanti con ritiro in negozio"),
	BONIFICO_BANCARIO("Bonifico bancario"),
	CARTA_DI_CREDITO("Carta di credito"),
	VAGLIO_POSTALE("Vaglio postale"),
	FINANZIARIA_CON_RATEIZZAZIONE("Finanziaria con rateizzazione");

	private String displayName;

	TipoModalitaPagamento(String displayName) {
        this.displayName = displayName;
    }

	@Override
    public String toString() { return displayName; }
}
