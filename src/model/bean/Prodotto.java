package model.bean;

import java.util.Date;

public class Prodotto {

	private int idCategoria;
	private int idProdotto;
	private String descrizione;
	private String marca;
	private String modello;
	private int codiceEAN;
	private double prezzo;
	private int disponibilita;
	private String URLProdotto;
	private double costiSpedizione;
	private int tempiConsegna;
	private String immagineProdotto;

	public Prodotto(int idCategoria, int idProdotto, String descrizione, String marca, String modello, int codiceEAN,
			double prezzo, int disponibilita, String uRLProdotto, double costiSpedizione, int tempiConsegna,
			String immagineProdotto) {
		super();
		this.idCategoria = idCategoria;
		this.idProdotto = idProdotto;
		this.descrizione = descrizione;
		this.marca = marca;
		this.modello = modello;
		this.codiceEAN = codiceEAN;
		this.prezzo = prezzo;
		this.disponibilita = disponibilita;
		URLProdotto = uRLProdotto;
		this.costiSpedizione = costiSpedizione;
		this.tempiConsegna = tempiConsegna;
		this.immagineProdotto = immagineProdotto;
	}

	public Prodotto() {
		super();
	}

	public Prodotto(int idCategoria, int idProdotto) {
		super();
		this.idCategoria = idCategoria;
		this.idProdotto = idProdotto;
	}

	public int getIdCategoria() {
		return idCategoria;
	}

	public void setIdCategoria(int idCategoria) {
		this.idCategoria = idCategoria;
	}

	public int getIdProdotto() {
		return idProdotto;
	}

	public void setIdProdotto(int idProdotto) {
		this.idProdotto = idProdotto;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModello() {
		return modello;
	}

	public void setModello(String modello) {
		this.modello = modello;
	}

	public int getCodiceEAN() {
		return codiceEAN;
	}

	public void setCodiceEAN(int codiceEAN) {
		this.codiceEAN = codiceEAN;
	}

	public double getPrezzo() {
		return prezzo;
	}

	public void setPrezzo(double prezzo) {
		this.prezzo = prezzo;
	}

	public int getDisponibilita() {
		return disponibilita;
	}

	public void setDisponibilita(int disponibilita) {
		this.disponibilita = disponibilita;
	}

	public String getURLProdotto() {
		return URLProdotto;
	}

	public void setURLProdotto(String uRLProdotto) {
		URLProdotto = uRLProdotto;
	}

	public double getCostiSpedizione() {
		return costiSpedizione;
	}

	public void setCostiSpedizione(double costiSpedizione) {
		this.costiSpedizione = costiSpedizione;
	}

	public int getTempiConsegna() {
		return tempiConsegna;
	}

	public void setTempiConsegna(int tempiConsegna) {
		this.tempiConsegna = tempiConsegna;
	}

	public String getImmagineProdotto() {
		return immagineProdotto;
	}

	public void setImmagineProdotto(String immagineProdotto) {
		this.immagineProdotto = immagineProdotto;
	}

	@Override
	public String toString() {
		return "Prodotto [idCategoria=" + idCategoria + ", idProdotto=" + idProdotto + ", descrizione=" + descrizione
				+ ", marca=" + marca + ", modello=" + modello + ", codiceEAN=" + codiceEAN + ", prezzo=" + prezzo
				+ ", disponibilita=" + disponibilita + ", URLProdotto=" + URLProdotto + ", costiSpedizione="
				+ costiSpedizione + ", tempiConsegna=" + tempiConsegna + ", immagineProdotto=" + immagineProdotto + "]\n";
	}

	
}
