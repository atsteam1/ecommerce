package model.bean;

import java.sql.Date;

public class Ordine {

	private int idOrdine;
	private int idCliente;
	private String indirizziOrdini;
	private String idModalitaPagamento;
	private Date dataOrdine;
	private double totaleOrdine;
	
	public Ordine(int idOrdine, int idCliente, String indirizziOrdini, String idModalitaPagamento, Date dataOrdine,
			double totaleOrdine) {
		super();
		this.idOrdine = idOrdine;
		this.idCliente = idCliente;
		this.indirizziOrdini = indirizziOrdini;
		this.idModalitaPagamento = idModalitaPagamento;
		this.dataOrdine = dataOrdine;
		this.totaleOrdine = totaleOrdine;
	}
	
	
	public Ordine() {
		super();
		
	}


	public int getIdOrdine() {
		return idOrdine;
	}
	public void setIdOrdine(int idOrdine) {
		this.idOrdine = idOrdine;
	}
	public int getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}
	public String getIndirizziOrdini() {
		return indirizziOrdini;
	}
	public void setIndirizziOrdini(String indirizziOrdini) {
		this.indirizziOrdini = indirizziOrdini;
	}
	public String getIdModalitaPagamento() {
		return idModalitaPagamento;
	}
	public void setIdModalitaPagamento(String idModalitaPagamento) {
		this.idModalitaPagamento = idModalitaPagamento;
	}
	public Date getDataOrdine() {
		return dataOrdine;
	}
	public void setDataOrdine(Date dataOrdine) {
		this.dataOrdine = dataOrdine;
	}
	public double getTotaleOrdine() {
		return totaleOrdine;
	}
	public void setTotaleOrdine(double totaleOrdine) {
		this.totaleOrdine = totaleOrdine;
	}


	@Override
	public String toString() {
		return "Ordine [idOrdine=" + idOrdine + ", idCliente=" + idCliente + ", indirizziOrdini=" + indirizziOrdini
				+ ", idModalitaPagamento=" + idModalitaPagamento + ", dataOrdine=" + dataOrdine + ", totaleOrdine="
				+ totaleOrdine + "]";
	}
	
	
}
