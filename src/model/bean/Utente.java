package model.bean;

public class Utente {

	private int id;
	private String username;
	private String password;
	private boolean amministratore;
	
	public Utente(int id, String username, String password, boolean amministratore) {
		super();
		this.id = id;
		this.username = username;
		this.password = password;
		this.amministratore = amministratore;
	}
	public Utente() {
		super();
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAmministratore() {
		return amministratore;
	}
	
	public String getAmministratore(){
		if(this.amministratore)
			return "s";
		else
			return "n";
	}
	
	public void setAmministratore(String s) {
		
		if(s.equals("s")) {
			this.amministratore = true;
		}else {
			this.amministratore = false;
		}
	}

}
