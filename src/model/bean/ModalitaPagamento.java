package model.bean;

public class ModalitaPagamento {

	private String tipoMod;

	public String getTipoMod() {
		return tipoMod;
	}

	public void setTipoMod(String tipoMod) {
		this.tipoMod = tipoMod;
	}

	@Override
	public String toString() {
		return "ModalitaPagamento [tipoMod=" + tipoMod + "]";
	}

}
