package model.bean;

public class ComposizioneOrdine {

	private int idOrdine;
	private int idProdotto;
	private int quantita;

	public int getIdOrdine() {
		return idOrdine;
	}

	public void setIdOrdine(int idOrdine) {
		this.idOrdine = idOrdine;
	}

	public int getIdProdotto() {
		return idProdotto;
	}

	public void setIdProdotto(int idProdotto) {
		this.idProdotto = idProdotto;
	}

	public int getQuantita() {
		return quantita;
	}

	public void setQuantita(int quantita) {
		this.quantita = quantita;
	}

	@Override
	public String toString() {
		return "ComposizioneOrdine [idOrdine=" + idOrdine + ", idProdotto=" + idProdotto + ", quantita=" + quantita
				+ "]";
	}

}
