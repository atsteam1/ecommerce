package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.bean.Amministratore;
import model.bean.Cliente;
import model.bean.Ordine;

public class DAOOrdineImpl implements DAOOrdine {

	@Override
	public void insert(Ordine o) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("insert into ordini values (IDO_sequence.nextval,?,?,?,?,?)",
				new String[] { "IDO" });

		ps.setString(1, o.getIdModalitaPagamento());
		ps.setDouble(2, o.getTotaleOrdine());
		ps.setDate(3, o.getDataOrdine());
		ps.setInt(4, o.getIdCliente());
		ps.setString(5, o.getIndirizziOrdini());
		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next()) {
			System.out.println(rs.getInt(1));
			o.setIdOrdine(rs.getInt(1));
		}

	}

	@Override
	public Ordine select(int idOrdine) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from ordini where IDO = ?");
		ps.setInt(1, idOrdine);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			Ordine o = new Ordine();
			o.setIdOrdine(rs.getInt("IDO"));
			o.setIdModalitaPagamento(rs.getString("IDMOD_FK"));
			o.setTotaleOrdine(rs.getDouble("TOTALEORDINE"));
			o.setIdCliente(rs.getInt("IDC_FK"));
			o.setIndirizziOrdini(rs.getString("INDIRIZZO"));
			return o;
		}

		return null;
	}

	@Override
	public void delete(int idOrdine) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("delete from utente where IDO = ?");

		ps.setInt(1, idOrdine);
		ps.executeUpdate();

	}

	@Override
	public List<Ordine> selectByIdCliente(int idCliente) throws Exception {
		List<Ordine> lista = new ArrayList<>();
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from ordini where IDC = ?");
		ps.setInt(1, idCliente);
		ResultSet rs = ps.executeQuery();
		
		while (rs.next()) {
			
			Ordine o = new Ordine();
			o.setIdOrdine(rs.getInt("IDO"));
			o.setIdModalitaPagamento(rs.getString("IDMOD_FK"));
			o.setTotaleOrdine(rs.getDouble("TOTALEORDINE"));
			o.setIdCliente(rs.getInt("IDC_FK"));
			o.setIndirizziOrdini(rs.getString("INDIRIZZO"));

			lista.add(o);

		}

		return lista;

	}

	@Override
	public void update(Ordine o) throws Exception {
		Connection conn = SingletonConnection.getConnection();

		PreparedStatement ps = conn.prepareStatement(
				"update ordini set IDMOD_FK = ?, TotaleOrdine = ?, DataOrdine = ?, IDC_FK = ?, Indirizzo = ?");

		ps.setString(1, o.getIdModalitaPagamento());
		ps.setDouble(2, o.getTotaleOrdine());
		ps.setDate(3, o.getDataOrdine());
		ps.setInt(4, o.getIdCliente());
		ps.setString(5, o.getIndirizziOrdini());

		ps.executeUpdate();

	}

}
