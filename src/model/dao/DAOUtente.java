package model.dao;

import java.util.List;

import model.bean.Utente;
import model.bean.Amministratore;
import model.bean.Cliente;
import model.bean.Ordine;

public interface DAOUtente {

	public void insert(Cliente c) throws Exception;

	public void delete(int id) throws Exception;

	public void update(Cliente c) throws Exception;

	public Utente select(int id) throws Exception;

	public List<Cliente> selectAll() throws Exception;

	public List<Cliente> selectAff(boolean affidabilitą) throws Exception;
	
	public Cliente selectByUsername(String username) throws Exception;
	
	public Amministratore selectByUsernameAmm(String username) throws Exception;
	
	public Amministratore selectAmm(int id) throws Exception;
}
