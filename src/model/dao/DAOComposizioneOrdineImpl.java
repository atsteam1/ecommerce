package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import model.bean.ComposizioneOrdine;

public class DAOComposizioneOrdineImpl implements DAOComposizioneOrdine {

	@Override
	public void insert(ComposizioneOrdine co) throws Exception {

		String sql = "insert into composizioniordini values(?, ?, ?)";

		Connection connection = SingletonConnection.getConnection();

		PreparedStatement ps = connection.prepareStatement(sql);

		int idOrdine = co.getIdOrdine();
		int idProdotto = co.getIdProdotto();
		int quantita = co.getQuantita();

		ps.setInt(1, idOrdine);
		ps.setInt(2, idProdotto);
		ps.setInt(3, quantita);

		ps.executeUpdate();
	}

	@Override
	public ComposizioneOrdine select(int idOrdine, int idProdotto) throws Exception {

		String sql = "select * from composizioniordini where IDO_FK = ? and IDP_FK = ?";

		Connection connection = SingletonConnection.getConnection();

		PreparedStatement ps = connection.prepareStatement(sql);

		ps.setInt(1, idOrdine);
		ps.setInt(2, idProdotto);

		ResultSet rs = ps.executeQuery();

		ComposizioneOrdine composizioneOrdine = null;

		if (rs.next()) {
			int quantita = rs.getInt("Quantita");

			composizioneOrdine = new ComposizioneOrdine();

			composizioneOrdine.setIdOrdine(idOrdine);
			composizioneOrdine.setIdProdotto(idProdotto);
			composizioneOrdine.setQuantita(quantita);
		}

		return composizioneOrdine;
	}

}
