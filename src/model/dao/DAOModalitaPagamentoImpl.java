package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.bean.ModalitaPagamento;

public class DAOModalitaPagamentoImpl implements DAOModalitaPagamento {

	@Override
	public List<ModalitaPagamento> selectAll() throws Exception {

		String sql = "select * from TipoModalitaPagamento";

		Connection connection = SingletonConnection.getConnection();

		PreparedStatement ps = connection.prepareStatement(sql);

		List<ModalitaPagamento> modalitaPagamento = new ArrayList<>();

		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			ModalitaPagamento mp = new ModalitaPagamento();

			mp.setTipoMod(rs.getString("TipoMod"));

			modalitaPagamento.add(mp);
		}

		return modalitaPagamento;
	}

}
