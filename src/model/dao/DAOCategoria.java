package model.dao;

import java.util.List;
import model.bean.Categoria;

public interface DAOCategoria {

	public void insert(Categoria categoria)throws Exception;
	
	public void delete(int idCategoria)throws Exception;
	
	public void update(Categoria categoria)throws Exception;
	
	public Categoria select(int idCategoria) throws Exception;
	
	public List<Categoria> selectAll()throws Exception;
	

	
	
	
}
