package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.bean.Amministratore;
import model.bean.Cliente;
import model.bean.Utente;

public class DAOUtenteImpl implements DAOUtente {

//	public void insert(Amministratore c) throws Exception {
//		Connection conn = SingletonConnection.getConnection();
//		Amministratore amm = new Amministratore();
//		PreparedStatement ps = conn.prepareStatement("insert into utente (IDCliente,Amministratore,Username,Password) values (utenti_sequence.nextval,'s',?,?)",new String[] { "IDC" });
//
//		ps.setString(1, amm.getUsername());
//		ps.setString(2, amm.getPassword());
//
//		ps.executeUpdate();
//
//		ResultSet rs = ps.getGeneratedKeys();
//		while (rs.next()) {
//			c.setId(rs.getInt(1));
//		}
//	}

	@Override
	public void insert(Cliente c) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		System.out.println("ps ");
		PreparedStatement ps = conn.prepareStatement("insert into utente values (utenti_sequence.nextval,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",new String[] { "IDCliente" });
		System.out.println("after ps");
		ps.setString(1, c.getTipologia());
		ps.setString(2, c.getAffidabile());
		ps.setString(3, "n");
		ps.setString(4, c.getUsername());
		ps.setString(5, c.getEmail());
		ps.setInt(6, c.getCellulare());
		ps.setInt(7, c.getTelefono());
		ps.setString(8, c.getIndirizzoFatturazione());
		ps.setString(9, c.getNome());
		ps.setString(10, c.getCognome());
		ps.setString(11, c.getCf());
		ps.setString(12, c.getPassword());
		ps.setString(13, c.getIndirizzoCliente());
		ps.setString(14, c.getPartitaIva());

		ps.executeUpdate();
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next()) {
			c.setId(rs.getInt(1));
			System.out.println(rs.getInt(1));
		}
	}

	@Override
	public void delete(int id) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("delete from utente where IDCliente = ? and Ammministratore='n'");

		ps.setInt(1, id);
		ps.executeUpdate();

	}

	
	public void update(Amministratore a) throws Exception{
		Connection conn = SingletonConnection.getConnection();
		Amministratore amm = new Amministratore();
		PreparedStatement ps = conn.prepareStatement(
				"update utente set Amministratore = ?, Username = ?, Password = ? where IDCliente = ?");

		ps.setString(1,"n");
		ps.setString(2, amm.getUsername());
		ps.setString(3, amm.getPassword());
		ps.setInt(4, amm.getId());

		ps.executeUpdate();
	}
	@Override
	public void update(Cliente cliente) throws Exception {
		Connection conn = SingletonConnection.getConnection();
			System.out.println("ps");
			PreparedStatement ps = conn.prepareStatement(
					"update utente set Tipologia = ?, Affidabile = ?, Amministratore = ?, Username = ?, Email = ?, Cellulare = ?, Telefono = ?, IndirizzoFatturazione = ?, Nome = ?, Cognome = ?, PIVA = ?, CF = ?, Password = ?, IndirizziClienti = ? where IDCliente = ?");
			System.out.println("afte ps");
			ps.setInt(15, cliente.getId());
			System.out.println("id");
			ps.setString(1, cliente.getTipologia());
			ps.setString(2, cliente.getAffidabile());
			ps.setString(3, "n");
			ps.setString(4, cliente.getUsername());
			ps.setString(5, cliente.getEmail());
			ps.setInt(6, cliente.getCellulare());
			ps.setInt(7, cliente.getTelefono());
			ps.setString(8, cliente.getIndirizzoFatturazione());
			ps.setString(9, cliente.getNome());
			ps.setString(10, cliente.getCognome());
			ps.setString(11, cliente.getPartitaIva());
			ps.setString(12, cliente.getCf());
			ps.setString(13, cliente.getPassword());
			ps.setString(14, cliente.getIndirizzoCliente());
			ps.executeUpdate();
	}

	public Amministratore selectAmm(int id) throws Exception {
		Connection conn = SingletonConnection.getConnection();

		PreparedStatement ps = conn
				.prepareStatement("select * from utente where IDCliente = ? and Amministratore ='s'");

		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		// pu� tornare zero o una riga, se non c'� non entra nell'if
		Amministratore c = null;
		if (rs.next()) {

			c = new Amministratore();// creiamo un utente vuoto

			c.setId(rs.getInt("IDCliente"));

			c.setUsername(rs.getString("Username"));

			c.setPassword(rs.getString("Password"));

			c.setAmministratore(rs.getString("Amministratore"));
		}
		return c;
	}

	@Override
	public Cliente select(int id) throws Exception {
		Connection conn = SingletonConnection.getConnection();

		PreparedStatement ps = conn.prepareStatement("select * from utente where IDCliente = ? and Amministratore='n'");

		ps.setInt(1, id);
		ResultSet rs = ps.executeQuery();
		// pu� tornare zero o una riga, se non c'� non entra nell'if
		if (rs.next()) {

			Cliente c = new Cliente();// creiamo un utente vuoto

			c.setId(rs.getInt("IDCliente"));

			c.setUsername(rs.getString("Username"));

			c.setPassword(rs.getString("Password"));

			c.setAmministratore(rs.getString("Amministratore"));

			c.setTipologia(rs.getString("Tipologia"));

			c.setAffidabile(rs.getString("Affidabile"));

			c.setAmministratore(rs.getString("Amministratore"));

			c.setUsername(rs.getString("Username"));

			c.setEmail(rs.getString("Email"));

			c.setCellulare(rs.getInt("Cellulare"));

			c.setTelefono(rs.getInt("Telefono"));

			c.setIndirizzoFatturazione(rs.getString("IndirizzoFatturazione"));

			c.setNome(rs.getString("Nome"));

			c.setCognome(rs.getString("Cognome"));

			c.setPartitaIva(rs.getString("PIVA"));

			c.setCf(rs.getString("CF"));

			c.setPassword(rs.getString("Password"));

			c.setIndirizzoCliente(rs.getString("IndirizziClienti"));
			return c;
		}

		return null;
	}

	@Override
	public List<Cliente> selectAll() throws Exception {
		List<Cliente> lista = new ArrayList<>();
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from utente where Amministratore = 'n'");
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {
			// recuperiamo gli elementi, costruiamo l'oggetto cliente e lo
			// mettiamo nella lista
			Cliente c = new Cliente(); // creiamo un cliente vuoto
			c.setId(rs.getInt("IDCliente"));
			c.setTipologia(rs.getString("Tipologia"));
			c.setAffidabile(rs.getString("Affidabile"));
			c.setAmministratore(rs.getString("Amministratore"));
			c.setUsername(rs.getString("Username"));
			c.setEmail(rs.getString("Email"));
			c.setCellulare(rs.getInt("Cellulare"));
			c.setTelefono(rs.getInt("Telefono"));
			c.setIndirizzoFatturazione(rs.getString("IndirizzoFatturazione"));
			c.setNome(rs.getString("Nome"));
			c.setCognome(rs.getString("Cognome"));
			c.setPartitaIva(rs.getString("PIVA"));
			c.setCf(rs.getString("CF"));
			c.setPassword(rs.getString("Password"));
			c.setIndirizzoCliente(rs.getString("IndirizziClienti"));

			lista.add(c);

		}
		return lista;

	}

	@Override
	public List<Cliente> selectAff(boolean affidabilita) throws Exception {
		String aff;
		if(affidabilita)
			 aff="s";
		else
			aff="n";
		List<Cliente> lista = new ArrayList<>();
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn
				.prepareStatement("select * from utente where affidabilit� = ?  and Amministratore = 'n'");
		ps.setString(1, aff);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Cliente c = new Cliente();// creiamo un utente vuoto
			c.setId(rs.getInt("IDCliente"));
			c.setTipologia(rs.getString("Tipologia"));
			c.setAffidabile(rs.getString("Affidabile"));
			c.setAmministratore(rs.getString("Amministratore"));
			c.setUsername(rs.getString("Username"));
			c.setEmail(rs.getString("Email"));
			c.setCellulare(rs.getInt("Cellulare"));
			c.setTelefono(rs.getInt("Telefono"));
			c.setIndirizzoFatturazione(rs.getString("IndirizzoFatturazione"));
			c.setNome(rs.getString("Nome"));
			c.setCognome(rs.getString("Cognome"));
			c.setPartitaIva(rs.getString("PIVA"));
			c.setCf(rs.getString("CF"));
			c.setPassword(rs.getString("Password"));
			c.setIndirizzoCliente(rs.getString("IndirizziClienti"));

			lista.add(c);

		}

		return lista;

	}

	public Cliente selectByUsername(String username) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn
				.prepareStatement("select * from utente where username = ? and Amministratore = 'n'");
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			Cliente c = new Cliente();// creiamo un cliente vuoto
			c.setId(rs.getInt("IDCliente"));
			c.setTipologia(rs.getString("Tipologia"));
			c.setAffidabile(rs.getString("Affidabile"));
			c.setAmministratore(rs.getString("Amministratore"));
			c.setUsername(rs.getString("Username"));
			c.setEmail(rs.getString("Email"));
			c.setCellulare(rs.getInt("Cellulare"));
			c.setTelefono(rs.getInt("Telefono"));
			c.setIndirizzoFatturazione(rs.getString("IndirizzoFatturazione"));
			c.setNome(rs.getString("Nome"));
			c.setCognome(rs.getString("Cognome"));
			c.setPartitaIva(rs.getString("PIVA"));
			c.setCf(rs.getString("CF"));
			c.setPassword(rs.getString("Password"));
			c.setIndirizzoCliente(rs.getString("IndirizziClienti"));

			return c;

		}

		return null;
	}

	public Amministratore selectByUsernameAmm(String username) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn
				.prepareStatement("select * from utente where username = ? and Amministratore = 's'");
		ps.setString(1, username);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {

			Amministratore a = new Amministratore();// creiamo un amministratore
													// vuoto
			a.setId(rs.getInt("IDCliente"));
			a.setAmministratore(rs.getString("Amministratore"));
			a.setUsername(rs.getString("Username"));
			a.setPassword(rs.getString("Password"));

			return a;

		}

		return null;
	}
	
	
//	public static void main(String[] args){
//		Cliente c= new Cliente(6, "fda","fde", false, true,"p", "dafg", "daga", "afgdsafgasf", "fadgag", "part","dfsf", "dfafadg", 3245523,3514, "adfa");
//		c.setUsername("prova della prova");
//		System.out.println(c);
//		DAOUtente du=new DAOUtenteImpl();
//		Amministratore a= new Amministratore(234, "admin", "admin", true);
//		try {
////			c=(Cliente) du.select(1);
////			System.out.println(c.getId());
////		du.insert(c);
////			c.setUsername("prova2");
//			c=du.selectByUsername("update");
//			System.out.println(c.getUsername());
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//	}
}