package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.bean.Prodotto;
import model.dto.DTOCriterio;
import model.dto.DTOCriterio.ChiaveRicerca;

public class DAOProdottoImpl implements DAOProdotto {

	@Override
	public void update(Prodotto p) throws Exception {

		Connection conn = SingletonConnection.getConnection();

		PreparedStatement ps = conn.prepareStatement(
				"update prodotti set DESCRIZIONE = ?, MARCA = ?, MODELLO = ?, CODICEEAN = ?, IMAGE_PATH = ?, PREZZO = ? , DISPONIBILITA = ?, URLPRODOTTO = ?, COSTISPEDIZIONE = ?, TEMPICONSEGNA = ?, IDCAT_FK = ? where IDP = ?");

		ps.setString(1, p.getDescrizione());
		ps.setString(2, p.getMarca());
		ps.setString(3, p.getModello());
		ps.setInt(4, p.getCodiceEAN());
		ps.setString(5, p.getImmagineProdotto());
		ps.setDouble(6, p.getPrezzo());
		ps.setInt(7, p.getDisponibilita());
		ps.setString(8, p.getURLProdotto());
		ps.setDouble(9, p.getCostiSpedizione());
		ps.setInt(10, p.getTempiConsegna());
		ps.setInt(11, p.getIdCategoria());
		ps.setInt(12, p.getIdProdotto());
		ps.executeUpdate();
	}

	@Override
	public void delete(int idProdotto) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("delete from prodotti where IDP = ?");
		ps.setInt(1, idProdotto);

		ps.executeUpdate();

	}

	@Override
	public void insert(Prodotto p) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement(
				"insert into prodotti values (Prodotti_sequence.nextval,?,?,?,?,?,?,?,?,?,?,?)",
				new String[] { "IDP" });
		ps.setString(1, p.getDescrizione());
		ps.setString(2, p.getMarca());
		ps.setString(3, p.getModello());
		ps.setInt(4, p.getCodiceEAN());
		ps.setString(5, p.getImmagineProdotto());
		ps.setDouble(6, p.getPrezzo());
		ps.setInt(7, p.getDisponibilita());
		ps.setString(8, p.getURLProdotto());
		ps.setDouble(9, p.getCostiSpedizione());
		ps.setInt(10, p.getTempiConsegna());
		ps.setInt(11, p.getIdCategoria());

		ps.executeUpdate();
		// PreparedStatement ps2 = conn.prepareStatement("select max(IDP) from
		// prodotti");
		// ResultSet rs = ps2.executeQuery();
		// while (rs.next()) {
		// System.out.println(rs.getInt(1));
		// p.setIdProdotto(rs.getInt(1));
		// }
		ResultSet rs = ps.getGeneratedKeys();
		while (rs.next()) {
			System.out.println(rs.getInt(1));
			p.setIdProdotto(rs.getInt(1));
		}

	}

	@Override

	public List<Prodotto> select(DTOCriterio c) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		String where = "";

		if (c != null) {
			where = "and " + c.getChiaveRicerca().toString() + " = '" + c.getValoreRicerca() + "'";
		}

		PreparedStatement ps = conn.prepareStatement(
				"select prodotti.* from prodotti, categorie where prodotti.idcat_fk = categorie.idcat " + where);
		ResultSet rs = ps.executeQuery();
		ArrayList<Prodotto> lista = new ArrayList<>();

		while (rs.next()) {

			Prodotto p = new Prodotto();
			p.setIdProdotto(rs.getInt("IDP"));
			p.setDescrizione(rs.getString("DESCRIZIONE"));
			p.setMarca(rs.getString("MARCA"));
			p.setModello(rs.getString("MODELLO"));
			p.setCodiceEAN(rs.getInt("CODICEEAN"));
			p.setImmagineProdotto(rs.getString("IMAGE_PATH"));
			p.setPrezzo(rs.getDouble("PREZZO"));
			p.setDisponibilita(rs.getInt("DISPONIBILITA"));
			p.setURLProdotto(rs.getString("URLPRODOTTO"));
			p.setCostiSpedizione(rs.getDouble("COSTISPEDIZIONE"));
			p.setTempiConsegna(rs.getInt("TEMPICONSEGNA"));
			p.setIdCategoria(rs.getInt("IDCAT_FK"));

			lista.add(p);

		}

		return lista;
	}

	@Override
	public Prodotto select(int idProdotto) throws Exception {
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from prodotti where IDP = ?");
		ps.setInt(1, idProdotto);
		ResultSet rs = ps.executeQuery();

		if (rs.next()) {
			Prodotto p = new Prodotto();
			p.setIdProdotto(rs.getInt("IDP"));
			p.setDescrizione(rs.getString("DESCRIZIONE"));
			p.setMarca(rs.getString("MARCA"));
			p.setModello(rs.getString("MODELLO"));
			p.setCodiceEAN(rs.getInt("CODICEEAN"));
			p.setImmagineProdotto(rs.getString("IMAGE_PATH"));
			p.setPrezzo(rs.getDouble("PREZZO"));
			p.setDisponibilita(rs.getInt("DISPONIBILITA"));
			p.setURLProdotto(rs.getString("URLPRODOTTO"));
			p.setCostiSpedizione(rs.getDouble("COSTISPEDIZIONE"));
			p.setTempiConsegna(rs.getInt("TEMPICONSEGNA"));
			p.setIdCategoria(rs.getInt("IDCAT_FK"));

			return p;
		}

		return null;
	}

	@Override
	public List<Prodotto> selectAll(int idCategoria) throws Exception {
		List<Prodotto> lista = new ArrayList<>();

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from prodotti where IDCAT_FK = ?");
		ps.setInt(1, idCategoria);
		ResultSet rs = ps.executeQuery();

		while (rs.next()) {

			Prodotto p = new Prodotto();
			p.setIdProdotto(rs.getInt("IDP"));
			p.setDescrizione(rs.getString("DESCRIZIONE"));
			p.setMarca(rs.getString("MARCA"));
			p.setModello(rs.getString("MODELLO"));
			p.setCodiceEAN(rs.getInt("CODICEEAN"));
			p.setImmagineProdotto(rs.getString("IMAGE_PATH"));
			p.setPrezzo(rs.getDouble("PREZZO"));
			p.setDisponibilita(rs.getInt("DISPONIBILITA"));
			p.setURLProdotto(rs.getString("URLPRODOTTO"));
			p.setCostiSpedizione(rs.getDouble("COSTISPEDIZIONE"));
			p.setTempiConsegna(rs.getInt("TEMPICONSEGNA"));
			p.setIdCategoria(rs.getInt("IDCAT_FK"));

			lista.add(p);

		}

		return lista;

	}

	@Override
	public void deleteAll(int idCategoria) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("delete from prodotti where IDCAT_FK = ?");
		ps.setInt(1, idCategoria);

		ps.executeUpdate();

	}

	public static void main(String[] args) {
		DAOProdotto dao = new DAOProdottoImpl();

		try {
			System.out.println(dao.select(new DTOCriterio(ChiaveRicerca.NOMECAT, "Moda")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
