package model.dao;

import java.util.ArrayList;
import java.util.List;

import model.bean.Prodotto;
import model.dto.DTOCriterio;

public interface DAOProdotto {

	void update(Prodotto prodotto) throws Exception;

	void delete(int idProdotto) throws Exception;

	void insert(Prodotto prodotto) throws Exception;

	Prodotto select(int idProdotto) throws Exception;
	List<Prodotto> select(DTOCriterio c) throws Exception;
	
	List<Prodotto> selectAll(int idCategoria) throws Exception;

	void deleteAll(int idCategoria) throws Exception;
}
