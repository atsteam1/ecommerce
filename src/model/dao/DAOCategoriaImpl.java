package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.bean.Categoria;

public class DAOCategoriaImpl implements DAOCategoria {

	@Override
	public void insert(Categoria categoria) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("insert into Categorie values (categotria_sequence.nextval,?)",new String[] {"IDCAT"});

		ps.setString(1, categoria.getNome());

		ps.executeUpdate();
		 ResultSet rs=ps.getGeneratedKeys();
		 while(rs.next()){
		 System.out.println(rs.getInt(1));
		 categoria.setIdCategoria(rs.getInt(1));
		 }

	}

	@Override
	public void delete(int idCategoria) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("delete from Categorie where IDCAT = ?");

		ps.setInt(1, idCategoria);

		ps.executeUpdate();

	}

	@Override
	public void update(Categoria categoria) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("update Categorie set NomeCat = ? where IDCAT = ?");

		ps.setString(1, categoria.getNome());
		ps.setInt(2, categoria.getIdCategoria());

		ps.executeUpdate();

	}

	@Override
	public Categoria select(int idCategoria) throws Exception {

		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from Categorie where IDCAT = ?");

		ps.setInt(1, idCategoria);

		ResultSet rs = ps.executeQuery();

		Categoria categoria = new Categoria();

		if (rs.next()) {
			categoria.setIdCategoria(idCategoria);
			categoria.setNome(rs.getString("NomeCat"));

		}
		return categoria;

	}

	@Override
	public List<Categoria> selectAll() throws Exception {
		
		Connection conn = SingletonConnection.getConnection();
		PreparedStatement ps = conn.prepareStatement("select * from Categorie");

		ResultSet rs = ps.executeQuery();
		
		ArrayList<Categoria> categorie = new ArrayList<>();
		
		while(rs.next()){
		
			Categoria categoria = new Categoria();
			categoria.setIdCategoria(rs.getInt("IDCAT"));
			categoria.setNome(rs.getString("NomeCat"));
			
			categorie.add(categoria);
		
		}
		return categorie;
	}

}
