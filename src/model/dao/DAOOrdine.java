package model.dao;

import java.util.List;

import model.bean.Ordine;

public interface DAOOrdine {

	public void insert(Ordine o) throws Exception;
	
	public void update(Ordine o) throws Exception;
	
	public Ordine select(int idOrdine) throws Exception;
	
	public void delete(int idOrdine) throws Exception;
	
	public List<Ordine> selectByIdCliente(int idCliente)throws Exception;
	
}
