package model.dao;

import model.bean.ComposizioneOrdine;

public interface DAOComposizioneOrdine {

	/**
	 * Inserisce una nuova composizione ordine.
	 * 
	 * @param co
	 *            - la composizione ordine da inserire
	 * @throws Exception
	 */
	void insert(ComposizioneOrdine co) throws Exception;

	/**
	 * Seleziona la composizione ordine identificata dall'idComposizioneOrdine.
	 * 
	 * @param idOrdine
	 *            - l'identificativo dell'ordine
	 * @param idProdotto
	 *            - l'identificativo del prodotto
	 * @return la composizione ordine cercata
	 * @throws Exception
	 */
	ComposizioneOrdine select(int idOrdine, int idProdotto) throws Exception;
}
