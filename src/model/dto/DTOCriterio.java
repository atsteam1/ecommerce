package model.dto;

public class DTOCriterio {

	public enum ChiaveRicerca{
		
		NOMECAT,MODELLO,MARCA;
	}
	
	private ChiaveRicerca chiaveRicerca;//modello, marca
	private String valoreRicerca;

	public ChiaveRicerca getChiaveRicerca() {
		return chiaveRicerca;
	}

	public DTOCriterio(ChiaveRicerca chiaveRicerca, String valoreRicerca) {
		super();
		this.chiaveRicerca = chiaveRicerca;
		this.valoreRicerca = valoreRicerca;
	}

	public void setChiaveRicerca(ChiaveRicerca chiaveRicerca) {
		this.chiaveRicerca = chiaveRicerca;
	}

	public String getValoreRicerca() {
		return valoreRicerca;
	}

	public void setValoreRicerca(String valoreRicerca) {
		this.valoreRicerca = valoreRicerca;
	}
}
