package model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import model.bean.Amministratore;
import model.bean.Categoria;
import model.bean.Cliente;
import model.bean.Ordine;
import model.bean.Prodotto;

import model.bean.Amministratore;
import model.bean.Categoria;
import model.bean.Cliente;
import model.bean.Ordine;
import model.bean.Prodotto;
import model.dao.DAOCategoria;
import model.dao.DAOCategoriaImpl;
import model.dao.DAOOrdine;
import model.dao.DAOOrdineImpl;
import model.dao.DAOProdotto;
import model.dao.DAOProdottoImpl;
import model.dao.DAOUtente;
import model.dao.DAOUtenteImpl;

@Service
public class ServiceAmministratoreImpl implements ServiceAmministratore {

	@Override
	public Amministratore controlloAdmin(String username, String password) throws Exception {
		DAOUtente du = new DAOUtenteImpl();
		Amministratore b = null;

		b = (Amministratore) du.selectByUsernameAmm(username);

		if (b != null) {
			
			if(b.getPassword() == null) {
				return b;
			}
			
			if (b.getPassword().equals(password))

				return b;

			else
				return null;
		}
		return null;
	}

	@Override
	public List<Cliente> visualizzaClienti() throws Exception {
		DAOUtente du = new DAOUtenteImpl();
		ArrayList<Cliente> ac = new ArrayList<>();

		ac = (ArrayList<Cliente>) du.selectAll();

		return ac;
	}

	@Override
	public List<Cliente> visualizzaAffidabili() throws Exception {
		DAOUtente du = new DAOUtenteImpl();
		ArrayList<Cliente> ac = new ArrayList<>();

		ac = (ArrayList<Cliente>) du.selectAff(true);

		return ac;
	}

	@Override
	public List<Cliente> visualizzaNonAffidabili() throws Exception {
		DAOUtente du = new DAOUtenteImpl();
		ArrayList<Cliente> ac = new ArrayList<>();

		ac = (ArrayList<Cliente>) du.selectAff(false);

		return ac;
	}

	@Override
	public void midificaAffidabilita(int idCliente, boolean Affidabilitą) throws Exception {
		DAOUtente du = new DAOUtenteImpl();
		Cliente ac = null;

		ac = (Cliente) du.select(idCliente);
		ac.setAffidabile(Affidabilitą);

	}

	@Override
	public void eliminaCliente(int idCliente) throws Exception {
		DAOUtente du = new DAOUtenteImpl();

		du.delete(idCliente);
	}

	@Override
	public Cliente visualizzaCliente(int idCliente) throws Exception {
		DAOUtente du = new DAOUtenteImpl();

		Cliente ce = (Cliente) du.select(idCliente);

		return ce;
	}

	@Override
	public List<Ordine> VisualizzaOrdiniCliente(int idCliente) throws Exception {
		DAOOrdine dO = new DAOOrdineImpl();

		List<Ordine> lo = dO.selectByIdCliente(idCliente);

		return lo;
	}

	@Override
	public void modificaCliente(Cliente c) throws Exception {
		DAOUtente du = new DAOUtenteImpl();

		du.update(c);
	}

	@Override
	public void inserisciProdotto(Prodotto p) throws Exception {
		DAOProdotto dp = new DAOProdottoImpl();

		dp.insert(p);
	}

	@Override
	public void eliminaProdotto(int idProdotto) throws Exception {
		DAOProdotto dp = new DAOProdottoImpl();

		dp.delete(idProdotto);
	}

	@Override
	public void eliminaProdottiCategoria(int idCategoria) throws Exception {
		DAOProdotto dp = new DAOProdottoImpl();

		dp.deleteAll(idCategoria);
	}

	@Override
	public void modificaProdotto(Prodotto p) throws Exception {
		DAOProdotto dp = new DAOProdottoImpl();

		dp.update(p);
	}

	@Override
	public void inserisciCategoria(Categoria c) throws Exception {
		DAOCategoria dc = new DAOCategoriaImpl();

		dc.insert(c);
	}

	@Override
	public void eliminaCategoria(int idCategoria) throws Exception {
		DAOCategoria dc = new DAOCategoriaImpl();

		dc.delete(idCategoria);
	}

	@Override
	public void modificaCategoria(Categoria g) throws Exception {
		DAOCategoria dc = new DAOCategoriaImpl();

		dc.update(g);
	}

}
