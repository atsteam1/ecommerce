package model.service;

import java.util.List;

import model.bean.Amministratore;
import model.bean.Categoria;
import model.bean.Cliente;
import model.bean.Ordine;
import model.bean.Prodotto;

public interface ServiceAmministratore {
 Amministratore controlloAdmin(String username,String passwor) throws Exception;
 List<Cliente> visualizzaClienti()throws Exception;
 List<Cliente> visualizzaAffidabili()throws Exception;
 List<Cliente> visualizzaNonAffidabili()throws Exception;
 void midificaAffidabilita(int idCliente,boolean Affidabilitą)throws Exception;
 void eliminaCliente(int idCliente) throws Exception;
 Cliente visualizzaCliente(int idCliente) throws Exception;
 List<Ordine>VisualizzaOrdiniCliente(int idCliente) throws Exception;
 void modificaCliente(Cliente c) throws Exception;
 
 void inserisciProdotto(Prodotto p) throws Exception;
 void eliminaProdotto(int idProdotto) throws Exception;
 void eliminaProdottiCategoria(int idCategoria) throws Exception;
 void modificaProdotto(Prodotto p) throws Exception;
 void inserisciCategoria(Categoria c) throws Exception;
 void eliminaCategoria(int idCategoria) throws Exception;
 void modificaCategoria(Categoria g) throws Exception;
}
