package model.service;

import java.util.List;

import model.bean.Categoria;
import model.bean.Cliente;
import model.bean.Ordine;
import model.bean.Prodotto;
import model.dto.DTOCriterio;

public interface ServiceCliente {

	void registraCliente(Cliente c) throws Exception;

	Cliente verificaCredenziali(String username, String password) throws Exception;

	void modificaCliente(Cliente c) throws Exception;

	Cliente visualizzaCliente(int idCliente) throws Exception;

	List<Ordine> visualizzaOrdini(int idCliente) throws Exception;

	Ordine visualizzaOrdine(int idOrdine) throws Exception;

	List<Categoria> visualizzaCategorie() throws Exception;

	List<Prodotto> ricercaProdotto(Prodotto nomeProdotto, DTOCriterio c)throws Exception;

	void modificaMarcatura(Cliente c, boolean affidabile) throws Exception;
	
	void effettuaOrdine(Ordine ordine) throws Exception;
	
	void modificaOrdine(Ordine ordine) throws Exception;
	
	void cancellaOrdine(Ordine ordine) throws Exception;
}
