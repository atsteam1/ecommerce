package model.service;

public interface ServiceUtente {
	
	public boolean controlloCredenziali(String username, String password) throws Exception;

	public void modificaPassword(String newPassword) throws Exception;
	
	public void modificaUsername(String newUsername) throws Exception;
	
}
