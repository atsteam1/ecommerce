package model.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import model.bean.Categoria;
import model.bean.Cliente;
import model.bean.Ordine;
import model.bean.Prodotto;
import model.dao.DAOCategoria;
import model.dao.DAOCategoriaImpl;
import model.dao.DAOOrdine;
import model.dao.DAOOrdineImpl;
import model.dao.DAOProdotto;
import model.dao.DAOProdottoImpl;
import model.dao.DAOUtente;
import model.dao.DAOUtenteImpl;
import model.dto.DTOCriterio;

@Service
public class ServiceClienteImpl implements ServiceCliente {

	@Override
	public void modificaCliente(Cliente c) throws Exception {
		DAOUtente daou = new DAOUtenteImpl();
		daou.update(c);
	}

	@Override
	public Cliente visualizzaCliente(int idCliente) throws Exception {
		DAOUtente daou = new DAOUtenteImpl();

		return (Cliente) daou.select(idCliente);
	}

	@Override
	public List<Ordine> visualizzaOrdini(int idCliente) throws Exception {

		ArrayList<Ordine> alo = new ArrayList<>();
		DAOOrdine daoo = new DAOOrdineImpl();
		alo = (ArrayList<Ordine>) daoo.selectByIdCliente(idCliente);

		return alo;
	}

	@Override
	public Ordine visualizzaOrdine(int idOrdine) throws Exception {
		DAOOrdine daoo = new DAOOrdineImpl();

		return daoo.select(idOrdine);

	}

	@Override
	public List<Categoria> visualizzaCategorie() throws Exception {

		ArrayList<Categoria> alcat = new ArrayList<>();
		DAOCategoria daoc = new DAOCategoriaImpl();
		alcat = (ArrayList<Categoria>) daoc.selectAll();

		return alcat;
	}

	@Override
	public List<Prodotto> ricercaProdotto(Prodotto nomeProdotto, DTOCriterio c) throws Exception {

		ArrayList<Prodotto> prodotti = new ArrayList<>();
		DAOProdotto daop = new DAOProdottoImpl();

		prodotti = (ArrayList<Prodotto>) daop.select(c);

		return prodotti;
	}

	@Override
	public void modificaMarcatura(Cliente c, boolean affidabile) throws Exception {

		c.setAffidabile(affidabile);
	}

	@Override
	public void registraCliente(Cliente c) throws Exception {

		DAOUtente daoo = new DAOUtenteImpl();

		daoo.insert(c);
	}

	@Override
	public Cliente verificaCredenziali(String username, String password) throws Exception {

		DAOUtente daou = new DAOUtenteImpl();
		Cliente c = daou.selectByUsername(username);

		if (c != null) {
			
			if(c.getPassword() == null) {
				return c;
			}
			
			if (c.getPassword().equals(password)) {
				return c;

			} else {
				return null;

			}
		}
		return null;
	}

	@Override
	public void effettuaOrdine(Ordine ordine) throws Exception {

		DAOOrdine d = new DAOOrdineImpl();

		d.insert(ordine);

	}

	@Override
	public void modificaOrdine(Ordine ordine) throws Exception {
		DAOOrdine d = new DAOOrdineImpl();

		d.update(ordine);
	}

	@Override
	public void cancellaOrdine(Ordine ordine) throws Exception {
		DAOOrdine d = new DAOOrdineImpl();

		d.delete(ordine.getIdOrdine());
	}

}
