
create sequence utenti_sequence;

create table utente(
IDCliente number(20) primary key ,
Tipologia varchar(1),
Affidabile varchar(1),
Amministratore varchar(1) default 'n',
Username varchar(30) unique,
Email varchar(50),
Cellulare number(20),
Telefono number(20),
IndirizzoFatturazione varchar(60),
Nome varchar(20),
Cognome varchar(20),
PIVA varchar(20),
CF varchar(20),
Password varchar(20),
IndirizziClienti varchar(60),
constraint aff check(Affidabile='s' or Affidabile='n'),
constraint ammi check(Amministratore='s' or Amministratore='n'),
constraint TipologiaControll check (Tipologia='p' or Tipologia='a'),
constraint RagioneSocialeControll check(((Tipologia='a') and Cognome is null) or(Tipologia='p' and Cognome is not null))
);

create table TipoModalitaPagamento(
TipoMod varchar(50) primary key);
insert into TipoModalitaPagamento values('Contanti con ritiro in negozio');
insert into TipoModalitaPagamento values('Bonifico bancario');
insert into TipoModalitaPagamento values('Carta di credito');
insert into TipoModalitaPagamento values('Vaglia postale');
insert into TipoModalitaPagamento values('Finanziaria con rateizzazione');




create sequence IDO_sequence;

create table ordini (
IDO number(20) primary key,
IDMOD_FK varchar(50),
TotaleOrdine number(20,2),
DataOrdine Date,
IDC_FK number(20),
Indirizzo varchar(60),
constraint fk_ModPagamento foreign key (IDMOD_FK) references TipoModalitaPagamento(TipoMod),
constraint fk_utenti foreign key (IDC_FK) references utente(IDCliente),
constraint pagamento check ((not(IDMOD_FK='Contanti con ritiro in negozio')or indirizzo is null) or(not(IDMOD_FK!='Contanti con ritiro in negozio')or indirizzo is not null))
);


create sequence Categorie_sequence;

create table Categorie(
  IDCAT NUMBER(20) primary key,
  NomeCat varchar(30)
);


create sequence Prodotti_sequence;
create table Prodotti(
  IDP number(20)  primary key,
  Descrizione varchar(100),
  Marca varchar(20),
  Modello varchar(20),
  CodiceEAN number(20),
  Image_Path varchar(40),
  Prezzo Number(30,2),
  Disponibilita number(38),
  URLProdotto varchar(100),
  CostiSpedizione number(30,2),
  TempiConsegna number(20),
  IDCat_FK number(20),
  constraint idcat_fk foreign key (IDCat_FK) references Categorie(IDCAT)
);

create table ComposizioniOrdini(
  IDO_FK number(20),
  IDP_FK NUMBER(20),
  Quantita number(20),
  constraint primary_key primary key (IDO_FK,IDP_FK),
  constraint ido_fk foreign key (IDO_FK) references Ordini(IDO) ,
  constraint idp_fk foreign key (IDP_FK) references Prodotti(IDP)
);